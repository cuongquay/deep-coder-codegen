Generate NodeJS program from DSL
---

When you're done with DSL code generation, you can use this tool to generate the NodeJS program with TypeScript.


## Install CLI tool

This is a prebuilt program let you install into your machine as a command line internface:

```shellscript
$ npm install -g deep-coder-codegen
```

## Generate Code

Next, you need to prepare a sample test file.

***Sample test file: `examples/tennis/tennis.1.json`***

```javascript

[
    {"input":[[6,6,6],[3,2,3]], "output":3},
    {"input":[[6,6],[2,3]], "output":2},
    {"input":[[6,6],[1,2]], "output":2},
    {"input":[[1,6,5],[6,2,7]], "output":1},
    {"input":[[7,6],[6,3]], "output":2}
]

```

***Generage a program using deep-coder***

```shellscript
$ docker run -it -v $(PWD)/examples:/tmp/ cuongdd1/deep-coder ./generate.sh /tmp/tennis/tennis.1.json > program.dot.dsl
```

***Example DSL file: `program.dot.dsl`***

```dsl

a <- read_list
b <- read_list
c <- sum b
d <- zip_with * b a
e <- sum d

```

***Run the generator command line with arguments:***

```shellscript
$ deep-coder-codegen program.dot.dsl examples/tennis/tennis.2.json -o testapp
```

***Test the generated nodejs application ***

```shellscript
$ cd testapp && npm install && npm test && cd ..

> deep-coder-autogen@0.1.2 test /Users/cuongdd1/deep-coder-codegen/testapp
> node es6run.js

Sample:  { input: [ [ 1, 3, -5 ], [ -2, 4, 1 ] ], output: 5 }
 - Result: 5 - Output: 5
Sample:  { input: [ [ 1, 2, 3, 4, 5 ], [ 1, 0, 1, 0, 1 ] ], output: 9 }
 - Result: 9 - Output: 9
Sample:  { input: [ [ -6, 5, -4, 1, -8 ], [ -8, 4, -6, 10, 4 ] ],
  output: 70 }
 - Result: 70 - Output: 70
Sample:  { input: [ [ 9, -3, 2 ], [ 3, -6, 3 ] ], output: 51 }
 - Result: 51 - Output: 51
Sample:  { input: [ [ 1 ], [ 1 ] ], output: 1 }
 - Result: 1 - Output: 1

```

***Update the deep-coder engine and rebuild it***

1. Checkout the source code from github: 
```
git clone https://github.com/HiroakiMikami/deep-coder.git update-deep-coder
```
2. Copy the Dockerfile from deep-coder/Dockerfile into update-deep-coder/Dockerfile
3. Change the code and build the image: 
```Dockerfile
docker build . -t cuongdd1/deep-coder
```