// @ts-check
'use strict';

// used by helper functions / convertToArray's toString method
let thisFunc = encodeURIComponent;

const typeMaps = {
    nop: function (type, required, schema) {
        return type;
    },
    java: function (type, required, schema) {
        let result = type;
        if (!required) result += '?';
        return result;
    },
    javascript: function (type, required, schema) {
        let result = type;
        if (result === 'integer') result = 'number';
        return result;
    },
    typescript: function (type, required, schema) {
        let result = type;
        if (result === 'integer') result = 'number';
        if (result === 'array') {
            result = 'Array';
            if (schema.items && schema.items.type) {
                result += '<' + typeMap(schema.items.type, false, schema.items) + '>';
            }
        }
        return result;
    },
    go: function (type, required, schema) {
        let result = type;
        if (result === 'integer') result = 'int';
        if (result === 'boolean') result = 'bool';
        if (result === 'object') result = 'struct{}';
        if (result === 'array') {
            result = '[100]';
            if (schema.items && schema.items.type) {
                result += typeMap(schema.items.type, false, schema.items);
            }
        }
        return result;
    }
};

const reservedWords = {
    nop: [],
    go: ['type']
};

let typeMap = typeMaps.nop;
let reserved = reservedWords.nop;

function parseCodeLine(codeMappings, line, last) {
    var parts = line.split('<-')
    var code = last ? 'return ' : ('var ' + parts[0] + ' = ')
    var func = parts[1].trim().split(' ')
    var param = func[0]
    var tmp = codeMappings[param] || 'undefined()'
    if (func.length > 1) {
        var args = func.slice(1)
        for (var i = 0; i < args.length; i++) {
            tmp = tmp.replace('$' + (i + 1), args[i])
        }
    }
    code += tmp;
    code = code.replace('xMINy', 'Math.min(x,y)')
    code = code.replace('xMAXy', 'Math.max(x,y)')
    return {
        code: code,
        param: param
    }
}

function transform(codes, samples, defaults, callback) {
    let lang = (defaults.language || '').toLowerCase();
    if (typeMaps[lang]) typeMap = typeMaps[lang];
    if (reservedWords[lang]) reserved = reservedWords[lang];
    let obj = Object.assign({}, defaults);
    obj.samples = samples ? JSON.stringify(samples) : null;
    let length = codes.length
    let codeObject = codes.map((line, idx) => {
        return parseCodeLine(defaults.codeMappings, line, idx == length - 1)
    })
    obj.codes = codeObject.map((e)=> e.code).join('\n    ')
    obj.params = [...new Set(codeObject.map((e)=> e.param))].join(', ')

    obj.this = function () {
        console.warn('this called');
        return thisFunc(this.paramName);
    };
    obj.length = function () {
        return true;
    };
    obj.capitalize = function () {
        return true;
    };
    if (callback) callback(null, obj);
    return obj;
}

module.exports = {
    transform: transform
};
