Generate dataset
  Max-Length: 4
  Dataset-Size: 1000
Progress: 15600 / 1000
# Program
---
a <- read_int
b <- read_int
c <- read_int
d <- read_list
e <- take c d
f <- take a e
g <- take a f
h <- take b g
---
# Program
---
a <- read_int
b <- read_int
c <- read_list
d <- head c
e <- take d c
f <- take a e
g <- access b f
---
# Program
---
a <- read_int
b <- read_int
c <- read_list
d <- head c
e <- take d c
f <- take a e
g <- take b f
---
# Program
---
a <- read_int
b <- read_int
c <- read_list
d <- head c
e <- take d c
f <- take a e
g <- drop b f
---
# Program
---
a <- read_int
b <- read_int
c <- read_list
d <- head c
e <- take d c
f <- take b e
g <- drop a f
---
# Program
---
a <- read_int
b <- read_int
c <- read_list
d <- read_int
e <- take d c
f <- take a e
g <- take a f
h <- take b g
---
# Program
---
a <- read_int
b <- read_int
c <- read_list
d <- read_list
e <- head d
f <- take e c
g <- take a f
h <- access b g
---
# Program
---
a <- read_int
b <- read_int
c <- read_list
d <- read_list
e <- head d
f <- take e c
g <- take a f
h <- take b g
---
# Program
---
a <- read_int
b <- read_int
c <- read_list
d <- read_list
e <- head d
f <- take e c
g <- take a f
h <- drop b g
---
# Program
---
a <- read_int
b <- read_list
c <- head b
d <- take c b
e <- take c d
f <- access a e
---
# Program
---
a <- read_int
b <- read_list
c <- head b
d <- take c b
e <- take a d
f <- head e
---
# Program
---
a <- read_int
b <- read_list
c <- head b
d <- take c b
e <- take c d
f <- drop a e
---
# Program
---
a <- read_int
b <- read_list
c <- head b
d <- take c b
e <- take a d
---
# Program
---
a <- read_int
b <- read_list
c <- read_int
d <- head b
e <- take d b
f <- take a e
g <- access c f
---
# Program
---
a <- read_int
b <- read_list
c <- read_int
d <- head b
e <- take d b
f <- take a e
g <- take c f
---
# Program
---
a <- read_int
b <- read_list
c <- read_int
d <- head b
e <- take d b
f <- take a e
g <- drop c f
---
# Program
---
a <- read_int
b <- read_list
c <- read_int
d <- head b
e <- take d b
f <- take c e
g <- drop a f
---
# Program
---
a <- read_int
b <- read_list
c <- read_int
d <- read_int
e <- take d b
f <- take a e
g <- take a f
h <- take c g
---
# Program
---
a <- read_int
b <- read_list
c <- read_int
d <- read_list
e <- head d
f <- take e b
g <- take a f
h <- access c g
---
# Program
---
a <- read_int
b <- read_list
c <- read_int
d <- read_list
e <- head d
f <- take e b
g <- take a f
h <- take c g
---
# Program
---
a <- read_int
b <- read_list
c <- read_int
d <- read_list
e <- head d
f <- take e b
g <- take a f
h <- drop c g
---
# Program
---
a <- read_int
b <- read_list
c <- read_list
d <- head c
e <- take d c
f <- take a e
g <- zip_with + f b
---
# Program
---
a <- read_int
b <- read_list
c <- read_list
d <- head c
e <- take d c
f <- take a e
g <- zip_with - f b
---
# Program
---
a <- read_int
b <- read_list
c <- read_list
d <- head c
e <- take d c
f <- take a e
g <- zip_with - b f
---
# Program
---
a <- read_int
b <- read_list
c <- read_list
d <- head c
e <- take d c
f <- take a e
g <- zip_with * f b
---
# Program
---
a <- read_int
b <- read_list
c <- read_list
d <- read_int
e <- head c
f <- take e b
g <- take a f
h <- access d g
---
# Program
---
a <- read_int
b <- read_list
c <- read_list
d <- read_int
e <- head c
f <- take e b
g <- take a f
h <- take d g
---
# Program
---
a <- read_int
b <- read_list
c <- read_list
d <- read_int
e <- head c
f <- take e b
g <- take a f
h <- drop d g
---
# Program
---
a <- read_int
b <- read_list
c <- read_list
d <- read_list
e <- head d
f <- take e b
g <- take a f
h <- zip_with + g c
---
# Program
---
a <- read_list
b <- head a
---
# Program
---
a <- read_list
b <- head a
c <- head a
d <- take c a
e <- take b d
---
# Program
---
a <- read_list
b <- head a
c <- head a
d <- take c a
e <- drop b d
---
# Program
---
a <- read_list
b <- read_int
c <- head a
d <- take c a
e <- take c d
f <- access b e
---
# Program
---
a <- read_list
b <- read_int
c <- head a
d <- take c a
e <- take b d
f <- head e
---
# Program
---
a <- read_list
b <- read_int
c <- head a
d <- take c a
e <- take c d
f <- drop b e
---
# Program
---
a <- read_list
b <- read_int
c <- head a
d <- take c a
e <- take b d
---
# Program
---
a <- read_list
b <- read_int
c <- read_int
d <- head a
e <- take d a
f <- take b e
g <- access c f
---
# Program
---
a <- read_list
b <- read_int
c <- read_int
d <- head a
e <- take d a
f <- take b e
g <- take c f
---
# Program
---
a <- read_list
b <- read_int
c <- read_int
d <- head a
e <- take d a
f <- take b e
g <- drop c f
---
# Program
---
a <- read_list
b <- read_int
c <- read_int
d <- head a
e <- take d a
f <- take c e
g <- drop b f
---
# Program
---
a <- read_list
b <- read_int
c <- read_int
d <- read_int
e <- take d a
f <- take b e
g <- take b f
h <- take c g
---
# Program
---
a <- read_list
b <- read_int
c <- read_int
d <- read_list
e <- head d
f <- take e a
g <- take b f
h <- access c g
---
# Program
---
a <- read_list
b <- read_int
c <- read_int
d <- read_list
e <- head d
f <- take e a
g <- take b f
h <- take c g
---
# Program
---
a <- read_list
b <- read_int
c <- read_int
d <- read_list
e <- head d
f <- take e a
g <- take b f
h <- drop c g
---
# Program
---
a <- read_list
b <- read_int
c <- read_list
d <- head c
e <- take d c
f <- take b e
g <- zip_with + f a
---
# Program
---
a <- read_list
b <- read_int
c <- read_list
d <- head c
e <- take d c
f <- take b e
g <- zip_with - f a
---
# Program
---
a <- read_list
b <- read_int
c <- read_list
d <- head c
e <- take d c
f <- take b e
g <- zip_with - a f
---
# Program
---
a <- read_list
b <- read_int
c <- read_list
d <- head c
e <- take d c
f <- take b e
g <- zip_with * f a
---
# Program
---
a <- read_list
b <- read_int
c <- read_list
d <- read_int
e <- head c
f <- take e a
g <- take b f
h <- access d g
---
# Program
---
a <- read_list
b <- read_int
c <- read_list
d <- read_int
e <- head c
f <- take e a
g <- take b f
h <- take d g
---
# Program
---
a <- read_list
b <- read_int
c <- read_list
d <- read_int
e <- head c
f <- take e a
g <- take b f
h <- drop d g
---
# Program
---
a <- read_list
b <- read_int
c <- read_list
d <- read_list
e <- head d
f <- take e a
g <- take b f
h <- zip_with + g c
---
# Program
---
a <- read_list
b <- read_list
c <- head b
d <- head b
e <- take d a
f <- take c e
---
# Program
---
a <- read_list
b <- read_list
c <- head b
d <- head b
e <- take d a
f <- drop c e
---
# Program
---
a <- read_list
b <- read_list
c <- read_int
d <- head b
e <- take d b
f <- take c e
g <- zip_with + f a
---
# Program
---
a <- read_list
b <- read_list
c <- read_int
d <- head b
e <- take d b
f <- take c e
g <- zip_with - f a
---
# Program
---
a <- read_list
b <- read_list
c <- read_int
d <- head b
e <- take d b
f <- take c e
g <- zip_with - a f
---
# Program
---
a <- read_list
b <- read_list
c <- read_int
d <- head b
e <- take d b
f <- take c e
g <- zip_with * f a
---
# Program
---
a <- read_list
b <- read_list
c <- read_int
d <- read_int
e <- head b
f <- take e a
g <- take c f
h <- access d g
---
# Program
---
a <- read_list
b <- read_list
c <- read_int
d <- read_int
e <- head b
f <- take e a
g <- take c f
h <- take d g
---
# Program
---
a <- read_list
b <- read_list
c <- read_int
d <- read_int
e <- head b
f <- take e a
g <- take c f
h <- drop d g
---
# Program
---
a <- read_list
b <- read_list
c <- read_int
d <- read_list
e <- head d
f <- take e a
g <- take c f
h <- zip_with + g b
---
# Program
---
a <- read_list
b <- read_list
c <- read_list
d <- head c
e <- head b
f <- take e a
g <- access d f
---
# Program
---
a <- read_list
b <- read_list
c <- read_list
d <- head c
e <- head b
f <- take e a
g <- take d f
---
# Program
---
a <- read_list
b <- read_list
c <- read_list
d <- head c
e <- head b
f <- take e a
g <- drop d f
---
# Program
---
a <- read_list
b <- read_list
c <- read_list
d <- read_int
e <- head c
f <- take e a
g <- take d f
h <- zip_with + g b
---
# Program
---
a <- read_list
b <- read_list
c <- read_list
d <- read_list
e <- head d
f <- take e a
g <- zip_with + f b
h <- zip_with + g c
---