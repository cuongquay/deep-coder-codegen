Generate dataset
  Max-Length: 2
  Dataset-Size: 10000
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- head c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- last c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- minimum c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- maximum c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- sum c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- count >0 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- count <0 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- count %2 == 0 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- count %2 == 1 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- drop a c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- reverse c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- sort c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- map +1 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- map -1 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- map *(-1) c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- map *2 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- map *3 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- map *4 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- map /2 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- map /3 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- map /4 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- map **2 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- filter >0 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- filter <0 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- filter %2 == 0 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- filter %2 == 1 c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- zip_with - c c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- scanl1 + c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- scanl1 - c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- scanl1 * c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- scanl1 MIN c
---
# Program
---
a <- read_int
b <- read_list
c <- take a b
d <- scanl1 MAX c
---
# Program
---
a <- read_int
b <- read_list
c <- drop a b
---
# Program
---
a <- read_list
b <- head a
---
# Program
---
a <- read_list
b <- head a
c <- access b a
---
# Program
---
a <- read_list
b <- last a
---
# Program
---
a <- read_list
b <- last a
c <- access b a
---
# Program
---
a <- read_list
b <- minimum a
---
# Program
---
a <- read_list
b <- minimum a
c <- access b a
---
# Program
---
a <- read_list
b <- maximum a
---
# Program
---
a <- read_list
b <- maximum a
c <- access b a
---
# Program
---
a <- read_list
b <- reverse a
c <- sum b
---
# Program
---
a <- read_list
b <- head a
c <- take b a
---
# Program
---
a <- read_list
b <- head a
c <- drop b a
---
# Program
---
a <- read_list
b <- last a
c <- take b a
---
# Program
---
a <- read_list
b <- last a
c <- drop b a
---
# Program
---
a <- read_list
b <- minimum a
c <- take b a
---
# Program
---
a <- read_list
b <- minimum a
c <- drop b a
---
# Program
---
a <- read_list
b <- maximum a
c <- take b a
---
# Program
---
a <- read_list
b <- maximum a
c <- drop b a
---
# Program
---
a <- read_list
b <- reverse a
---
# Program
---
a <- read_list
b <- reverse a
c <- reverse b
---
# Program
---
a <- read_list
b <- reverse a
c <- sort b
---
# Program
---
a <- read_list
b <- reverse a
c <- map +1 b
---
# Program
---
a <- read_list
b <- reverse a
c <- map -1 b
---
# Program
---
a <- read_list
b <- reverse a
c <- map *(-1) b
---
# Program
---
a <- read_list
b <- reverse a
c <- map *2 b
---
# Program
---
a <- read_list
b <- reverse a
c <- map *3 b
---
# Program
---
a <- read_list
b <- reverse a
c <- map *4 b
---
# Program
---
a <- read_list
b <- reverse a
c <- map /2 b
---
# Program
---
a <- read_list
b <- reverse a
c <- map /3 b
---
# Program
---
a <- read_list
b <- reverse a
c <- map /4 b
---
# Program
---
a <- read_list
b <- reverse a
c <- map **2 b
---
# Program
---
a <- read_list
b <- reverse a
c <- filter >0 b
---
# Program
---
a <- read_list
b <- reverse a
c <- filter <0 b
---
# Program
---
a <- read_list
b <- reverse a
c <- filter %2 == 0 b
---
# Program
---
a <- read_list
b <- reverse a
c <- filter %2 == 1 b
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- head c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- last c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- minimum c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- maximum c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- sum c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- count >0 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- count <0 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- count %2 == 0 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- count %2 == 1 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- drop b c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- reverse c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- sort c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- map +1 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- map -1 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- map *(-1) c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- map *2 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- map *3 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- map *4 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- map /2 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- map /3 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- map /4 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- map **2 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- filter >0 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- filter <0 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- filter %2 == 0 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- filter %2 == 1 c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- zip_with - c c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- scanl1 + c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- scanl1 - c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- scanl1 * c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- scanl1 MIN c
---
# Program
---
a <- read_list
b <- read_int
c <- take b a
d <- scanl1 MAX c
---
# Program
---
a <- read_list
b <- read_int
c <- drop b a
---
# Program
---
a <- read_list
b <- read_list
c <- head b
d <- access c a
---
# Program
---
a <- read_list
b <- read_list
c <- head a
d <- access c b
---
# Program
---
a <- read_list
b <- read_list
c <- last b
d <- access c a
---
# Program
---
a <- read_list
b <- read_list
c <- last a
d <- access c b
---
# Program
---
a <- read_list
b <- read_list
c <- minimum b
d <- access c a
---
# Program
---
a <- read_list
b <- read_list
c <- minimum a
d <- access c b
---
# Program
---
a <- read_list
b <- read_list
c <- maximum b
d <- access c a
---
# Program
---
a <- read_list
b <- read_list
c <- maximum a
d <- access c b
---
# Program
---
a <- read_list
b <- read_list
c <- head b
d <- take c a
---
# Program
---
a <- read_list
b <- read_list
c <- head b
d <- drop c a
---
# Program
---
a <- read_list
b <- read_list
c <- head a
d <- take c b
---
# Program
---
a <- read_list
b <- read_list
c <- head a
d <- drop c b
---
# Program
---
a <- read_list
b <- read_list
c <- last b
d <- take c a
---
# Program
---
a <- read_list
b <- read_list
c <- last b
d <- drop c a
---
# Program
---
a <- read_list
b <- read_list
c <- last a
d <- take c b
---
# Program
---
a <- read_list
b <- read_list
c <- last a
d <- drop c b
---
# Program
---
a <- read_list
b <- read_list
c <- minimum b
d <- take c a
---
# Program
---
a <- read_list
b <- read_list
c <- minimum b
d <- drop c a
---
# Program
---
a <- read_list
b <- read_list
c <- minimum a
d <- take c b
---
# Program
---
a <- read_list
b <- read_list
c <- minimum a
d <- drop c b
---
# Program
---
a <- read_list
b <- read_list
c <- maximum b
d <- take c a
---
# Program
---
a <- read_list
b <- read_list
c <- maximum b
d <- drop c a
---
# Program
---
a <- read_list
b <- read_list
c <- maximum a
d <- take c b
---
# Program
---
a <- read_list
b <- read_list
c <- maximum a
d <- drop c b
---
# Program
---
a <- read_list
b <- read_list
c <- reverse b
d <- zip_with + c a
---
# Program
---
a <- read_list
b <- read_list
c <- reverse b
d <- zip_with - c a
---
# Program
---
a <- read_list
b <- read_list
c <- reverse b
d <- zip_with - a c
---
# Program
---
a <- read_list
b <- read_list
c <- reverse b
d <- zip_with * c a
---
# Program
---
a <- read_list
b <- read_list
c <- reverse b
d <- zip_with MIN c a
---
# Program
---
a <- read_list
b <- read_list
c <- reverse b
d <- zip_with MAX c a
---
# Program
---
a <- read_list
b <- read_list
c <- reverse a
d <- zip_with + c b
---
# Program
---
a <- read_list
b <- read_list
c <- reverse a
d <- zip_with - c b
---
# Program
---
a <- read_list
b <- read_list
c <- reverse a
d <- zip_with - b c
---
# Program
---
a <- read_list
b <- read_list
c <- reverse a
d <- zip_with * c b
---