# `tennis evaluation`
Receives two list, and outputs the inner product of two list.

$ docker run -it -v $(PWD)/examples:/tmp/ cuongdd1/deep-coder ./generate.sh /tmp/tennis/tennis.1.json > program.dot.dsl
$ docker run -it -v $(PWD)/examples:/tmp/ cuongdd1/deep-coder ./generate.sh /tmp/tennis/tennis.1.json model/model.dat 2 > program.dot.dsl

$ deep-coder-codegen program.dot.dsl examples/tennis/tennis.2.json -o testapp

$ cd testapp && npm install && npm test && cd ..


# `retrain tennis model`
Train and generate tennis match scores

```shellscript
docker run -it -v $(PWD)/examples/tennis:/tmp cuongdd1/deep-coder python3 ./python/learner.py /tmp/tennis/dataset.json /tmp/model.dat

docker run -it -v $(PWD)/examples/tennis:/tmp cuongdd1/deep-coder ./generate.sh /tmp/tennis/tennis.2.json /tmp/model.dat 2 dfs
```
