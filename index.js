#!/usr/bin/env node
'use strict';

const fs = require('fs');
const path = require('path');
const admzip = require('adm-zip');
const processor = require('./codegen.js');

var argv = require('yargs')
    .usage('cg [options] {[path]configName} {dslProgram} {samples}')
    .boolean('debug')
    .alias('d', 'debug')
    .describe('debug', 'Turn on debugging information in the model')
    .string('output')
    .alias('o', 'output')
    .describe('output', 'Specify output directory')
    .default('output', './runable/')
    .string('templates')
    .alias('t', 'templates')
    .describe('templates', 'Specify templates directory')
    .boolean('verbose')
    .describe('verbose', 'Increase verbosity')
    .alias('v', 'verbose')
    .version()
    .argv;

let configStr = 'deep-coder';
let configName = path.basename(configStr);
let configPath = path.dirname(configStr);
if (!configPath || (configPath === '.')) configPath = __dirname + '/configs';
let configFile = path.resolve(configPath, configName) + '.json';
let config = require(configFile);
let defName = argv._[0] || __dirname + '/program.dot.dsl';
let splName = argv._[1];

config.outputDir = argv.output;
config.templateDir = argv.templates;

function finish(err, result) {
    if (argv.zip) {
        // create archive
        var zip = new admzip();

        // add files directly
        for (let f in zipFiles) {
            zip.addFile(f, new Buffer(zipFiles[f]), 'Created with DeepCoder-CodeGen');
        }
        // write everything to disk
        zip.writeZip(path.join(config.outputDir, configName + '.zip'));
    }
}

function dslParse(p) {
    var results = [];
    var startIndex = p.indexOf('---');
    p = p.slice(startIndex);
    p = p.replace(/---/g, '').trim().replace(/ == /g,'==');
    results = p.split('\n');
    return results;
}

function main(p, s) {
    processor.main(p, s, config, configName, finish);
}

if (argv.verbose) {
    config.defaults.verbose = true;
    console.log('Loaded config ' + configName);
}
if (argv.debug) config.defaults.debug = true;

config.defName = defName
config.defaults.flat = true
let p = fs.readFileSync(defName, 'utf8');
let s = splName !== undefined ? fs.readFileSync(splName, 'utf8') : null;
main(dslParse(p), JSON.parse(s));
